using System;
using System.Windows.Forms;
using System.Drawing;

namespace FractalZoomer
{
	/// <summary>
	/// Summary description for DirectInheritance1.
	/// </summary>
	[Serializable] 
	public class DirectInheritance1 : TheWorldPainter
	{
		public DirectInheritance1(Control area, double ox, double oy, double s) : base(area, ox, oy, s) {}
		override public int CalculateOnePoint(double x, double y) 
		{
			return (int) (x*x*y + y*y*x);
			
		}
		override public Color LookUpColor(int n)
		{
			
			if(n == 500)return Color.Black;
			if(n > 400) return Color.Yellow;
			if(n > 300) return Color.Blue;
			if(n%2 == 0) return Color.Black;
			else return Color.FromArgb(Math.Abs(128+n)%256,Math.Abs(255-n)%255,Math.Abs(128-n)%255);
			
			//return n%2==0 ? Color.Black : Color.Red;

		}
	}
}
