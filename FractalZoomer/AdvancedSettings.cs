using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace FractalZoomer
{
	/// <summary>
	/// Summary description for AdvancedSettings.
	/// </summary>
	public class AdvancedSettings : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox origox;
		private System.Windows.Forms.TextBox origoy;
		private System.Windows.Forms.TextBox scale;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Button OkButton;
		private System.Windows.Forms.Button CancelButton0;
		private System.Windows.Forms.Label aaaaa;
		private System.Windows.Forms.TextBox maxn;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public AdvancedSettings(TheWorldPainter p)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			origox.Text = Convert.ToString(p.OrigoX);
			origoy.Text = Convert.ToString(p.OrigoY);
			scale.Text = Convert.ToString(p.Scale);
			
			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.origox = new System.Windows.Forms.TextBox();
			this.origoy = new System.Windows.Forms.TextBox();
			this.scale = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.OkButton = new System.Windows.Forms.Button();
			this.CancelButton0 = new System.Windows.Forms.Button();
			this.aaaaa = new System.Windows.Forms.Label();
			this.maxn = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// origox
			// 
			this.origox.Location = new System.Drawing.Point(208, 32);
			this.origox.Name = "origox";
			this.origox.Size = new System.Drawing.Size(112, 20);
			this.origox.TabIndex = 0;
			this.origox.Text = "";
			// 
			// origoy
			// 
			this.origoy.Location = new System.Drawing.Point(208, 64);
			this.origoy.Name = "origoy";
			this.origoy.Size = new System.Drawing.Size(112, 20);
			this.origoy.TabIndex = 1;
			this.origoy.Text = "";
			// 
			// scale
			// 
			this.scale.ImeMode = System.Windows.Forms.ImeMode.On;
			this.scale.Location = new System.Drawing.Point(208, 96);
			this.scale.Name = "scale";
			this.scale.Size = new System.Drawing.Size(112, 20);
			this.scale.TabIndex = 2;
			this.scale.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 32);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(104, 16);
			this.label1.TabIndex = 3;
			this.label1.Text = "Origo X";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 64);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(104, 16);
			this.label2.TabIndex = 4;
			this.label2.Text = "Origo Y";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 96);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(104, 16);
			this.label3.TabIndex = 5;
			this.label3.Text = "Scale";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Location = new System.Drawing.Point(16, 264);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(656, 216);
			this.groupBox1.TabIndex = 6;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Good Staring Values";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 48);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(568, 24);
			this.label5.TabIndex = 1;
			this.label5.Text = "More to come. A general hint, is to have a very small scale if you manage to find" +
				" a good spot to start at";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(568, 24);
			this.label4.TabIndex = 0;
			this.label4.Text = "Mandelbrot: origox = -0.73797607; origoy = 0.13291995; scale = 0,0000000317;";
			// 
			// OkButton
			// 
			this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.OkButton.Location = new System.Drawing.Point(408, 32);
			this.OkButton.Name = "OkButton";
			this.OkButton.TabIndex = 4;
			this.OkButton.Text = "OK";
			// 
			// CancelButton0
			// 
			this.CancelButton0.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelButton0.Location = new System.Drawing.Point(408, 64);
			this.CancelButton0.Name = "CancelButton0";
			this.CancelButton0.TabIndex = 5;
			this.CancelButton0.Text = "Cancel";
			// 
			// aaaaa
			// 
			this.aaaaa.Location = new System.Drawing.Point(16, 128);
			this.aaaaa.Name = "aaaaa";
			this.aaaaa.Size = new System.Drawing.Size(184, 16);
			this.aaaaa.TabIndex = 10;
			this.aaaaa.Text = "Max Iterations ";
			// 
			// maxn
			// 
			this.maxn.Location = new System.Drawing.Point(208, 128);
			this.maxn.Name = "maxn";
			this.maxn.Size = new System.Drawing.Size(112, 20);
			this.maxn.TabIndex = 3;
			this.maxn.Text = "";
			// 
			// AdvancedSettings
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(688, 485);
			this.Controls.Add(this.aaaaa);
			this.Controls.Add(this.maxn);
			this.Controls.Add(this.CancelButton0);
			this.Controls.Add(this.OkButton);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.scale);
			this.Controls.Add(this.origoy);
			this.Controls.Add(this.origox);
			this.Name = "AdvancedSettings";
			this.Text = "Advanced Settings";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion
		public string OrigoX
		{
			get
			{
				return origox.Text;
			}
		}
		public string OrigoY
		{
			get
			{
				return origoy.Text;
			}
		}
		public string MyScale
		{
			get
			{
				return scale.Text;
			}
		}
		public string MaxIterations
		{
			get
			{
				return maxn.Text;
			}
		}


	}
}
