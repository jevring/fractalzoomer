using System;
using System.Drawing;

namespace FractalZoomer
{
	/// <summary>
	/// Summary description for IterativePaintAlgorithm.
	/// </summary>
	[Serializable] 
	public abstract class IterativePaintAlgorithm
	{
		protected int Maxn = 500;
		
		public IterativePaintAlgorithm()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public int MaxIterations 
		{
			get
			{
				return Maxn;
			}
			set
			{
				Maxn = value;
			}
		}

		public Color BAW(int n) 
		{
			return (n == Maxn ? Color.Black : Color.White);
		}
	}
}
