using System;
using System.Drawing;

namespace FractalZoomer
{
	/// <summary>
	/// Summary description for Mandel.
	/// </summary>
	[Serializable] 
	public class Mandel : IterativePaintAlgorithm
	{
		int Closemax1 = 400;
		int Closemax2 = 300;
		public Mandel()
		{
		}
		public int MandelFunction(double cre, double cim)
		{
			Complex c = new Complex(cre,cim);
			Complex z = new Complex(0,0); 
			int n = 0;
			while(n < Maxn && z.Norm2 < 4)
			{
				n++;
				z = z*z+c;
			}
			return n;
		}
		public Color LookUpColor(int n)
		{
			
			if(n == Maxn) return Color.Black;
			else if(n < 0) return Color.Black;
			else if(n > Closemax1) return Color.Yellow;
			else if(n > Closemax2) return Color.Blue;
			//else return Color.FromArgb(n%256,255, n%255);
			else return Color.FromArgb(Math.Abs(128+n)%256,Math.Abs(255-n)%255,Math.Abs(128-n)%255);
		}

		
	}
}
