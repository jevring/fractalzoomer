using System;
using System.Drawing.Imaging;
using System.Threading;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace FractalZoomer
{
	/// <summary>
	/// Summary description for TheWorldPainter.
	/// </summary>
	[Serializable] public delegate int FDelegate(double x, double y); 
	[Serializable] public delegate Color ColorDelegate(int n);
	public struct Complex 
	{
		public double Re;
		public double Im;
		public Complex(double r, double i) 
		{
			Re = r;
			Im = i;
		}
		public Complex(double x)
			:this(System.Math.Cos(x), System.Math.Sin(x)) {}

		public double Norm2 
		{
			get 
			{
				return Math.Pow(Re,2) + Math.Pow(Im,2);
			}
		}
		public double Norm
		{
			get {return System.Math.Sqrt(Norm2); }
		}

		public static Complex operator + (Complex x, Complex y) 
		{
			return new Complex(x.Re + y.Re, x.Im + y.Im);
		}
		public static Complex operator + (Complex x, double y) 
		{
			return new Complex(x.Re + y, x.Im);
		}
		public static Complex operator + (double x, Complex y) 
		{
			return new Complex(y.Re + x, y.Im);
		}
		public static Complex operator - (Complex x, Complex y) 
		{
			return new Complex(x.Re - y.Re, x.Im - y.Im);
		}
		public static Complex operator - (Complex x, double y) 
		{
			return new Complex(x.Re - y, x.Im);
		}
		public static Complex operator - (double x, Complex y) 
		{
			return new Complex(y.Re - x, y.Im);
		}
		public static Complex operator * (Complex x, Complex y) 
		{
			double tRe;
			double tIm;
			tRe = x.Re * y.Re - x.Im * y.Im;
			tIm = x.Re * y.Im + x.Im * y.Re; 
			return new Complex(tRe, tIm);
		}
		public static Complex operator * (Complex x, double y) 
		{
			return new Complex(x.Re * y, x.Im * y);
		}
		public static Complex operator * (double x, Complex y) 
		{
			return new Complex(y.Re * x, y.Im * x);
		}

		public static Complex operator / (Complex x, Complex y) 
		{
			double t;
			t = y.Re * y.Re + y.Im * y.Im;
			double tRe;
			double tIm;
			
			tRe = (x.Re*y.Re + x.Im*y.Im)/t;
			tIm = (x.Im*y.Re - x.Re*y.Im)/t;
			return new Complex(tRe, tIm);
		}
		public static Complex operator / (Complex x, double y) 
		{
			return new Complex(x.Re / y, x.Im / y);
		}
		public static Complex operator / (double x, Complex y) 
		{
			double t;
			t = y.Re * y.Re + y.Im * y.Im;
			double tRe;
			double tIm;
			
			tRe = y.Re/t;
			tIm = y.Im/t;
			return new Complex(x*tRe, (0-x)*tIm);
		}


	}
	[Serializable]
	public class TheWorldPainter
	{
		[NonSerialized] Control paintBox;
		[NonSerialized] SolidBrush myBrush;
		double origox;
		double origoy;
		double scale;
		public FDelegate F; 
		public ColorDelegate Coloring;
		[NonSerialized] Thread computingThread;
		//private GraphicsState pic;

		public TheWorldPainter(Control area, double ox, double oy, double s) 
		{ 
			paintBox = area; 
			origox = ox; 
			origoy = oy; 
			scale = s; 
			myBrush = new SolidBrush(Color.Blue); 
			F = new FDelegate(this.CalculateOnePoint); 
			Coloring = new ColorDelegate(this.LookUpColor);
		}
		public struct PixelData
		{
			public PixelData(byte red, byte green, byte blue)
			{
				this.red = red;
				this.green = green;
				this.blue = blue;
			}

			public byte red;
			public byte green;
			public byte blue;
		}
		public static unsafe PixelData* PixelAt(int x, int y, IntPtr pBase, int width)
		{
			return (PixelData*) ((long)pBase + y * (width * sizeof(PixelData)) + x * sizeof(PixelData));	
		}
		public void PaintTheWorld()
		{	
			ClearTheWorld();
			Graphics g = paintBox.CreateGraphics();
			/*
			for(int ix = 0;ix < paintBox.Width; ix++)
				for(int iy = 0; iy < paintBox.Height;iy++)
				{
					double x = origox + ix*scale;
					double y = origoy + iy*scale;
					int n = F(x,y);
					myBrush.Color = Coloring(n);
					g.FillRectangle(myBrush,ix,iy,1,1);
				}
			*/
			

			// NEW RENDERER
			long start = System.DateTime.Now.Ticks;
			
			Bitmap bmp = new Bitmap(paintBox.Width, paintBox.Height);
			BitmapData bmpBits = bmp.LockBits(new Rectangle(0,0,paintBox.Width, paintBox.Height),ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
			IntPtr pBase = bmpBits.Scan0;
			
			for(int ix = 0; ix < paintBox.Width; ix++) 
			{
				
				if (ix % 10 == 0) 
				{
					string status = "Progress: " + ((double)ix / (double)paintBox.Width) * 100 + "%";
					status = status.Substring(0, (status.Length <= 16 ? status.Length : 16));
					//Console.Out.WriteLine(status);
					g.FillRectangle(new SolidBrush(Color.White), 250, 100, 250,45);
					g.DrawString(status, new Font("Verdana", 24, FontStyle.Bold), new SolidBrush(Color.Black), 100, 100 );
				}
				
				for(int iy = 0; iy < paintBox.Height; iy++)
				{
					// x och y h�r �r v�rldskoordinater!
					double x = origox + ix*scale;
					double y = origoy + iy*scale;
					int n = F(x,y);
					Color c = Coloring(n);
					unsafe 
					{
						PixelData* pixel = PixelAt(ix,iy, pBase, paintBox.Width);
						pixel->red = Convert.ToByte(c.R);
						pixel->green = Convert.ToByte(c.G);
						pixel->blue = Convert.ToByte(c.B);
					}
				}
			}
			bmp.UnlockBits(bmpBits);
					
			((PictureBox)paintBox).Image = bmp;
			paintBox.Update();
			

			// OLD RENDERER
			/*
			int paintSize = 32;
			while(paintSize > 0)
			{
				for(int ix = 0; ix < paintBox.Width; ix = ix+paintSize)
				for(int iy = 0; iy < paintBox.Height; iy = iy + paintSize)
				{
					// x och y h�r �r v�rldskoordinater!
					double x = origox + ix*scale;
					double y = origoy + iy*scale;
					int n = F(x,y);
					myBrush.Color = Coloring(n);
					
					g.FillRectangle(myBrush,ix,iy,1,1);
				}
				paintSize = paintSize/2;
			}
			*/
			// save this so we can do a quick restore we we regain focus
			//pic = g.Save();
			long timeToRenderOneFrame = System.DateTime.Now.Ticks - start;
			Console.Out.WriteLine("fps: " + 1 / (timeToRenderOneFrame * 0.0000001));
			
		}
		/*		
		public int CalculateOnePoint(double x, double y)
		{
			//return (int)(x+y);			
			//return (int)( 20*(Math.Sin(x/5) + Math.Cos(y/5)));
			//return (int)( 20*(Math.Sin(x/5) * Math.Cos(y/5)));
			//return (int)(x*x/(1+y*y));
			// return (int)(50*Math.Sin(x*y)/(1+y*y)); // <- cool
			//return (int)(50*Math.Tan(x*y)/(1+y*y)); // <- coolare
			//return (int)(x*x + y*y);
			return (int) Math.Sqrt(x*x + y*y);
		}
		*/
		/*
		public int CalculateOnePoint(double x, double y)
		{
			double Re = 0;
			double Im = 0;
			int n = 0;
			while(n < 500 && Re*Re + Im*Im < 4)
			{
				double resave = Re;
				Re = Re*Re - Im*Im + x;
				Im = 2*resave*Im + y;
				n++;
			}
			return n;
		}
		*/
		virtual public int CalculateOnePoint(double x, double y) 
		{
			return (int) (x*x + y*y);
		}
		
		/*
				public Color LookUpColor(int n)
				{
					if (n < 0) return Color.Black;
					else return Color.FromArgb((n*40)%256,(n*50)%256,(n*60)%256);
				}
		*/
		virtual public Color LookUpColor(int n)
		{
			
			if(n == 500)return Color.Black;
			if(n > 400) return Color.Yellow;
			if(n > 300) return Color.Blue;
			if(n%2 == 0) return Color.Black;
			else return Color.FromArgb(Math.Abs(n)%256,255,255);
			
			//return n%2==0 ? Color.Black : Color.Red;

		}
		public Color Zebra(int n) 
		{
			return (n%2 == 1 ? Color.Black : Color.White);
		}
		public void Rescale(int x, int y, double newscale)
		{
			double oldx = origox + scale*x;
			double oldy = origoy + scale*y;
			scale = scale*newscale;
			origox = oldx - scale*paintBox.Width/2;
			origoy = oldy - scale*paintBox.Height/2;
		}
		public void ClearTheWorld() 
		{
			// todo: this has to reset the world coordinates, or something.
			if (myBrush == null) 
			{
				// since we're serializing the entire painter for unknown reasons, we have to do things like this to objects that aren't serializable
				myBrush = new SolidBrush(Color.White);
			}
			myBrush.Color = Color.White;
			Graphics g = paintBox.CreateGraphics(); 
			g.FillRectangle(myBrush,0,0, paintBox.Width, paintBox.Height); 
		}
		public void StartPainting()
		{
			StopPainting();
			computingThread = new Thread(new ThreadStart(this.PaintTheWorld));			
			computingThread.Start();
		}

		public void StopPainting()
		{
			if(computingThread != null) computingThread.Abort();
		}
		
		public double Scale 
		{
			get 
			{
				return scale;
			}
		}
		public double OrigoX 
		{
			get 
			{
				return origox;
			}
			set
			{
				origox = value;
			}
		}
		public double OrigoY 
		{
			get 
			{
				return origoy;
			}
			set
			{
				origoy = value;
			}
		}
		public double getWX(int n) 
		{
			return origox + (n*scale);
		}
		public double getWY(int n) 
		{
			return origoy + (n*scale);
		}
		public int GetOnePoint(int x, int y) 
		{
			return F(Convert.ToDouble(getWX(x)), Convert.ToDouble(getWY(y)));
		}
	}
}
