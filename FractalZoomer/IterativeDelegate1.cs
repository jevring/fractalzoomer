using System;
using System.Drawing;

namespace FractalZoomer
{
	/// <summary>
	/// Summary description for IterativeDelegate1.
	/// </summary>
	[Serializable] 
	public class IterativeDelegate1 : IterativePaintAlgorithm
	{
		int Closemax1 = 400;
		int Closemax2 = 300;
		public IterativeDelegate1()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public int Function(double cre, double cim)
		{
			Complex c = new Complex(cre,cim);
			Complex z = new Complex(0,0); 
			int n = 0;
			while(n < Maxn && z.Norm2 < 4)
			{
				n++;
				// flera exempel: http://www.lifesmith.com/technical.html#anchor271694
				//z =  (z*z*z*3)+(c*c);
				z=(z*z*z)-(z*z)+z+c;
				
			}
			return n;
		}
		public Color LookUpColor(int n)
		{
			if(n == Maxn) return Color.Black;
			else if(n < 0) return Color.Black;
			else if(n > Closemax1) return Color.Yellow;
			else if(n > Closemax2) return Color.Blue;
			else return Color.FromArgb(n%256,255, 255);
		}
	}
}
