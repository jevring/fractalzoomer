using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Soap;


namespace FractalZoomer
{
	/// <summary>
	/// Summary description for FormUI.
	/// </summary>
	public class FormUI : System.Windows.Forms.Form
	{
		#region Instance Variables
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem About;
		private System.Windows.Forms.MenuItem AboutFZ;
		private System.Windows.Forms.MenuItem FileSave;
		private System.Windows.Forms.MenuItem FileSaveAs;
		private System.Windows.Forms.MenuItem FileLoad;
		private System.Windows.Forms.MenuItem FileExit;
		private System.Windows.Forms.MenuItem FileNew;
		private System.Windows.Forms.Label MousePositionWorldLabel;
		private System.Windows.Forms.Label UpperLeftCornerLabel;
		private System.Windows.Forms.Label ScaleLabel;
		private System.Windows.Forms.Label MousePositionOnScreenLabel;
		private System.Windows.Forms.Label ValueOfFLabel;
		private System.Windows.Forms.PictureBox Image;
		private System.Windows.Forms.GroupBox InfoGroupBox;
		private System.Windows.Forms.MenuItem MenuFile;
		private System.Windows.Forms.MenuItem MenuOptions;
		private System.Windows.Forms.Label UpperLeftCornerDataLabel;
		private System.Windows.Forms.Label MousePositionInTheWorldDataLabel;
		private System.Windows.Forms.Label MousePositionOnScreenDataLabel;
		private System.Windows.Forms.Label ValueOfFDataLabel;
		private System.Windows.Forms.Label SelectedFunctionLabel;
		private System.Windows.Forms.Label SelectedFunctionDataLabel;
		private System.Windows.Forms.Label ScaleDataLabel;
		private Mandel mandel;


		private TheWorldPainter painter;
		private System.Windows.Forms.MenuItem MenuItemStart;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private IterativeDelegate1 id1;
		private DirectDelegate1 dd1;
		private System.Windows.Forms.Label SelectedColoringDataLabel;
		private System.Windows.Forms.Label SelectedColoringLabel;
		private MenuItem currentlyCheckedFunction = null;
		private MenuItem currentlyCheckedColoring = null;
		private MenuItem currentlyCheckedMouse = null;
		
		private double origox;
		private double origoy;
		private double scale;
		private double dragStartX;
		private double dragStartY;
		private System.Windows.Forms.MenuItem BlackAndWhiteColoring;
		private System.Windows.Forms.MenuItem ZebraColoring;
		private System.Windows.Forms.MenuItem FunctionMenuItem;
		private System.Windows.Forms.MenuItem IterativeFunctionMenuItem;
		private System.Windows.Forms.MenuItem MandelbrotFunctionMenuItem;
		private System.Windows.Forms.MenuItem IterativeDelegate1FunctionMenuItem;
		private System.Windows.Forms.MenuItem DirectFunctionMenuItem;
		private System.Windows.Forms.MenuItem DirectFunction1MenuItem;
		private System.Windows.Forms.MenuItem DirectFunction2MenuItem;
		private System.Windows.Forms.MenuItem ColoringMenuItem;
		private System.Windows.Forms.MenuItem StopPaintingMenuItem;
		private System.Windows.Forms.MenuItem MouseMenuItem;
		private System.Windows.Forms.MenuItem MouseZoomsMenuItem;
		private System.Windows.Forms.MenuItem MouseMovesMenuITem;
		private IterativePaintAlgorithm p;
		private System.Windows.Forms.MenuItem DefaultColoringMenuItem;
		private bool ZoomWithMouse = true;
		private System.Windows.Forms.MenuItem AdvancedSettingsMenuItem;
		private ColorDelegate defaultColorDelegateForCurrentPaintAlgorithm;
		private System.Windows.Forms.MenuItem RainbowColoring;
		private System.Windows.Forms.MenuItem MandelbrotDefaultColoring;
		private string SaveFileName = "";

		#endregion

		public FormUI()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			
			// todo: tell the user about cool starting values that he/she could set, somehow.
			// maybe that's for version 2.0
			//origox = -0.73797607; origoy = 0.13291995; scale = 3.17E-08;
			//origox = -0.73797607; origoy = 0.13291995; scale = 2D/this.Image.Height;
			origox = -3D; origoy = -2D; scale = 4.2/this.Image.Height;

			// start with a neat default painter. (should the user be able to specify scale and origo?)
			painter = new TheWorldPainter(this.Image, origox, origoy, scale);
			// is this allowed/does it work?
			MandelbrotFunctionMenuItem_Click(MandelbrotFunctionMenuItem, null);
			Image.MouseDown += new MouseEventHandler(DoMouseDown);
			Image.MouseUp += new MouseEventHandler(DoMouseUp);
			this.MouseWheel +=new MouseEventHandler(FormUI_MouseWheel);
			Image.MouseMove +=new MouseEventHandler(Image_MouseMove);
			
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Image = new System.Windows.Forms.PictureBox();
			this.InfoGroupBox = new System.Windows.Forms.GroupBox();
			this.SelectedColoringDataLabel = new System.Windows.Forms.Label();
			this.SelectedColoringLabel = new System.Windows.Forms.Label();
			this.SelectedFunctionDataLabel = new System.Windows.Forms.Label();
			this.SelectedFunctionLabel = new System.Windows.Forms.Label();
			this.ValueOfFDataLabel = new System.Windows.Forms.Label();
			this.MousePositionOnScreenDataLabel = new System.Windows.Forms.Label();
			this.MousePositionInTheWorldDataLabel = new System.Windows.Forms.Label();
			this.ScaleDataLabel = new System.Windows.Forms.Label();
			this.UpperLeftCornerDataLabel = new System.Windows.Forms.Label();
			this.ValueOfFLabel = new System.Windows.Forms.Label();
			this.MousePositionOnScreenLabel = new System.Windows.Forms.Label();
			this.ScaleLabel = new System.Windows.Forms.Label();
			this.UpperLeftCornerLabel = new System.Windows.Forms.Label();
			this.MousePositionWorldLabel = new System.Windows.Forms.Label();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.MenuFile = new System.Windows.Forms.MenuItem();
			this.FileNew = new System.Windows.Forms.MenuItem();
			this.FileLoad = new System.Windows.Forms.MenuItem();
			this.FileSave = new System.Windows.Forms.MenuItem();
			this.FileSaveAs = new System.Windows.Forms.MenuItem();
			this.FileExit = new System.Windows.Forms.MenuItem();
			this.MenuOptions = new System.Windows.Forms.MenuItem();
			this.FunctionMenuItem = new System.Windows.Forms.MenuItem();
			this.IterativeFunctionMenuItem = new System.Windows.Forms.MenuItem();
			this.MandelbrotFunctionMenuItem = new System.Windows.Forms.MenuItem();
			this.IterativeDelegate1FunctionMenuItem = new System.Windows.Forms.MenuItem();
			this.DirectFunctionMenuItem = new System.Windows.Forms.MenuItem();
			this.DirectFunction1MenuItem = new System.Windows.Forms.MenuItem();
			this.DirectFunction2MenuItem = new System.Windows.Forms.MenuItem();
			this.ColoringMenuItem = new System.Windows.Forms.MenuItem();
			this.BlackAndWhiteColoring = new System.Windows.Forms.MenuItem();
			this.ZebraColoring = new System.Windows.Forms.MenuItem();
			this.RainbowColoring = new System.Windows.Forms.MenuItem();
			this.DefaultColoringMenuItem = new System.Windows.Forms.MenuItem();
			this.MandelbrotDefaultColoring = new System.Windows.Forms.MenuItem();
			this.MouseMenuItem = new System.Windows.Forms.MenuItem();
			this.MouseZoomsMenuItem = new System.Windows.Forms.MenuItem();
			this.MouseMovesMenuITem = new System.Windows.Forms.MenuItem();
			this.AdvancedSettingsMenuItem = new System.Windows.Forms.MenuItem();
			this.About = new System.Windows.Forms.MenuItem();
			this.AboutFZ = new System.Windows.Forms.MenuItem();
			this.MenuItemStart = new System.Windows.Forms.MenuItem();
			this.StopPaintingMenuItem = new System.Windows.Forms.MenuItem();
			this.InfoGroupBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// Image
			// 
			this.Image.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.Image.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Image.Location = new System.Drawing.Point(16, 8);
			this.Image.Name = "Image";
			this.Image.Size = new System.Drawing.Size(720, 464);
			this.Image.TabIndex = 0;
			this.Image.TabStop = false;
			this.Image.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Image_MouseMove);
			// 
			// InfoGroupBox
			// 
			this.InfoGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.InfoGroupBox.Controls.Add(this.SelectedColoringDataLabel);
			this.InfoGroupBox.Controls.Add(this.SelectedColoringLabel);
			this.InfoGroupBox.Controls.Add(this.SelectedFunctionDataLabel);
			this.InfoGroupBox.Controls.Add(this.SelectedFunctionLabel);
			this.InfoGroupBox.Controls.Add(this.ValueOfFDataLabel);
			this.InfoGroupBox.Controls.Add(this.MousePositionOnScreenDataLabel);
			this.InfoGroupBox.Controls.Add(this.MousePositionInTheWorldDataLabel);
			this.InfoGroupBox.Controls.Add(this.ScaleDataLabel);
			this.InfoGroupBox.Controls.Add(this.UpperLeftCornerDataLabel);
			this.InfoGroupBox.Controls.Add(this.ValueOfFLabel);
			this.InfoGroupBox.Controls.Add(this.MousePositionOnScreenLabel);
			this.InfoGroupBox.Controls.Add(this.ScaleLabel);
			this.InfoGroupBox.Controls.Add(this.UpperLeftCornerLabel);
			this.InfoGroupBox.Controls.Add(this.MousePositionWorldLabel);
			this.InfoGroupBox.Location = new System.Drawing.Point(16, 488);
			this.InfoGroupBox.Name = "InfoGroupBox";
			this.InfoGroupBox.Size = new System.Drawing.Size(720, 208);
			this.InfoGroupBox.TabIndex = 1;
			this.InfoGroupBox.TabStop = false;
			this.InfoGroupBox.Text = "Info";
			// 
			// SelectedColoringDataLabel
			// 
			this.SelectedColoringDataLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.SelectedColoringDataLabel.Location = new System.Drawing.Point(192, 176);
			this.SelectedColoringDataLabel.Name = "SelectedColoringDataLabel";
			this.SelectedColoringDataLabel.Size = new System.Drawing.Size(512, 16);
			this.SelectedColoringDataLabel.TabIndex = 14;
			// 
			// SelectedColoringLabel
			// 
			this.SelectedColoringLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.SelectedColoringLabel.Location = new System.Drawing.Point(16, 176);
			this.SelectedColoringLabel.Name = "SelectedColoringLabel";
			this.SelectedColoringLabel.Size = new System.Drawing.Size(104, 16);
			this.SelectedColoringLabel.TabIndex = 13;
			this.SelectedColoringLabel.Text = "Selected Coloring:";
			// 
			// SelectedFunctionDataLabel
			// 
			this.SelectedFunctionDataLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.SelectedFunctionDataLabel.Location = new System.Drawing.Point(192, 152);
			this.SelectedFunctionDataLabel.Name = "SelectedFunctionDataLabel";
			this.SelectedFunctionDataLabel.Size = new System.Drawing.Size(512, 16);
			this.SelectedFunctionDataLabel.TabIndex = 12;
			// 
			// SelectedFunctionLabel
			// 
			this.SelectedFunctionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.SelectedFunctionLabel.Location = new System.Drawing.Point(16, 152);
			this.SelectedFunctionLabel.Name = "SelectedFunctionLabel";
			this.SelectedFunctionLabel.Size = new System.Drawing.Size(104, 16);
			this.SelectedFunctionLabel.TabIndex = 11;
			this.SelectedFunctionLabel.Text = "Selected Function :";
			// 
			// ValueOfFDataLabel
			// 
			this.ValueOfFDataLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ValueOfFDataLabel.Location = new System.Drawing.Point(192, 104);
			this.ValueOfFDataLabel.Name = "ValueOfFDataLabel";
			this.ValueOfFDataLabel.Size = new System.Drawing.Size(512, 16);
			this.ValueOfFDataLabel.TabIndex = 10;
			// 
			// MousePositionOnScreenDataLabel
			// 
			this.MousePositionOnScreenDataLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.MousePositionOnScreenDataLabel.Location = new System.Drawing.Point(192, 80);
			this.MousePositionOnScreenDataLabel.Name = "MousePositionOnScreenDataLabel";
			this.MousePositionOnScreenDataLabel.Size = new System.Drawing.Size(512, 16);
			this.MousePositionOnScreenDataLabel.TabIndex = 9;
			// 
			// MousePositionInTheWorldDataLabel
			// 
			this.MousePositionInTheWorldDataLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.MousePositionInTheWorldDataLabel.Location = new System.Drawing.Point(192, 56);
			this.MousePositionInTheWorldDataLabel.Name = "MousePositionInTheWorldDataLabel";
			this.MousePositionInTheWorldDataLabel.Size = new System.Drawing.Size(512, 16);
			this.MousePositionInTheWorldDataLabel.TabIndex = 8;
			// 
			// ScaleDataLabel
			// 
			this.ScaleDataLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.ScaleDataLabel.Location = new System.Drawing.Point(192, 128);
			this.ScaleDataLabel.Name = "ScaleDataLabel";
			this.ScaleDataLabel.Size = new System.Drawing.Size(512, 16);
			this.ScaleDataLabel.TabIndex = 7;
			// 
			// UpperLeftCornerDataLabel
			// 
			this.UpperLeftCornerDataLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.UpperLeftCornerDataLabel.Location = new System.Drawing.Point(192, 32);
			this.UpperLeftCornerDataLabel.Name = "UpperLeftCornerDataLabel";
			this.UpperLeftCornerDataLabel.Size = new System.Drawing.Size(512, 16);
			this.UpperLeftCornerDataLabel.TabIndex = 6;
			// 
			// ValueOfFLabel
			// 
			this.ValueOfFLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.ValueOfFLabel.Location = new System.Drawing.Point(16, 104);
			this.ValueOfFLabel.Name = "ValueOfFLabel";
			this.ValueOfFLabel.Size = new System.Drawing.Size(160, 16);
			this.ValueOfFLabel.TabIndex = 4;
			this.ValueOfFLabel.Text = "Value of F :";
			// 
			// MousePositionOnScreenLabel
			// 
			this.MousePositionOnScreenLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.MousePositionOnScreenLabel.Location = new System.Drawing.Point(16, 80);
			this.MousePositionOnScreenLabel.Name = "MousePositionOnScreenLabel";
			this.MousePositionOnScreenLabel.Size = new System.Drawing.Size(168, 16);
			this.MousePositionOnScreenLabel.TabIndex = 3;
			this.MousePositionOnScreenLabel.Text = "Mouse Position on Screen (x,y) :";
			// 
			// ScaleLabel
			// 
			this.ScaleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.ScaleLabel.Location = new System.Drawing.Point(16, 128);
			this.ScaleLabel.Name = "ScaleLabel";
			this.ScaleLabel.Size = new System.Drawing.Size(48, 16);
			this.ScaleLabel.TabIndex = 2;
			this.ScaleLabel.Text = "Scale: ";
			// 
			// UpperLeftCornerLabel
			// 
			this.UpperLeftCornerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.UpperLeftCornerLabel.Location = new System.Drawing.Point(16, 32);
			this.UpperLeftCornerLabel.Name = "UpperLeftCornerLabel";
			this.UpperLeftCornerLabel.Size = new System.Drawing.Size(136, 16);
			this.UpperLeftCornerLabel.TabIndex = 1;
			this.UpperLeftCornerLabel.Text = "Upper Left Corner (x,y) :";
			// 
			// MousePositionWorldLabel
			// 
			this.MousePositionWorldLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.MousePositionWorldLabel.Location = new System.Drawing.Point(16, 56);
			this.MousePositionWorldLabel.Name = "MousePositionWorldLabel";
			this.MousePositionWorldLabel.Size = new System.Drawing.Size(176, 16);
			this.MousePositionWorldLabel.TabIndex = 0;
			this.MousePositionWorldLabel.Text = "Mouse Position in the World (x,y) :";
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.MenuFile,
																					  this.MenuOptions,
																					  this.About,
																					  this.MenuItemStart,
																					  this.StopPaintingMenuItem});
			// 
			// MenuFile
			// 
			this.MenuFile.Index = 0;
			this.MenuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.FileNew,
																					 this.FileLoad,
																					 this.FileSave,
																					 this.FileSaveAs,
																					 this.FileExit});
			this.MenuFile.Text = "File";
			// 
			// FileNew
			// 
			this.FileNew.Index = 0;
			this.FileNew.Text = "New";
			// 
			// FileLoad
			// 
			this.FileLoad.Index = 1;
			this.FileLoad.Text = "Load";
			this.FileLoad.Click += new System.EventHandler(this.FileLoad_Click);
			// 
			// FileSave
			// 
			this.FileSave.Index = 2;
			this.FileSave.Text = "Save";
			this.FileSave.Click += new System.EventHandler(this.FileSave_Click);
			// 
			// FileSaveAs
			// 
			this.FileSaveAs.Index = 3;
			this.FileSaveAs.Text = "Save As";
			this.FileSaveAs.Click += new System.EventHandler(this.FileSaveAs_Click);
			// 
			// FileExit
			// 
			this.FileExit.Index = 4;
			this.FileExit.Text = "Exit";
			this.FileExit.Click += new System.EventHandler(this.FileExit_Click);
			// 
			// MenuOptions
			// 
			this.MenuOptions.Index = 1;
			this.MenuOptions.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						this.FunctionMenuItem,
																						this.ColoringMenuItem,
																						this.MouseMenuItem,
																						this.AdvancedSettingsMenuItem});
			this.MenuOptions.Text = "Options";
			// 
			// FunctionMenuItem
			// 
			this.FunctionMenuItem.Index = 0;
			this.FunctionMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																							 this.IterativeFunctionMenuItem,
																							 this.DirectFunctionMenuItem});
			this.FunctionMenuItem.Text = "Function";
			// 
			// IterativeFunctionMenuItem
			// 
			this.IterativeFunctionMenuItem.Index = 0;
			this.IterativeFunctionMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																									  this.MandelbrotFunctionMenuItem,
																									  this.IterativeDelegate1FunctionMenuItem});
			this.IterativeFunctionMenuItem.Text = "Iterative";
			// 
			// MandelbrotFunctionMenuItem
			// 
			this.MandelbrotFunctionMenuItem.Index = 0;
			this.MandelbrotFunctionMenuItem.Text = "Mandelbrot";
			this.MandelbrotFunctionMenuItem.Click += new System.EventHandler(this.MandelbrotFunctionMenuItem_Click);
			// 
			// IterativeDelegate1FunctionMenuItem
			// 
			this.IterativeDelegate1FunctionMenuItem.Index = 1;
			this.IterativeDelegate1FunctionMenuItem.Text = "Double Fractal";
			this.IterativeDelegate1FunctionMenuItem.Click += new System.EventHandler(this.IterativeDelegate1FunctionMenuItem_Click);
			// 
			// DirectFunctionMenuItem
			// 
			this.DirectFunctionMenuItem.Index = 1;
			this.DirectFunctionMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																								   this.DirectFunction1MenuItem,
																								   this.DirectFunction2MenuItem});
			this.DirectFunctionMenuItem.Text = "Direct";
			// 
			// DirectFunction1MenuItem
			// 
			this.DirectFunction1MenuItem.Index = 0;
			this.DirectFunction1MenuItem.Text = "Direct1 (dd1)";
			this.DirectFunction1MenuItem.Click += new System.EventHandler(this.DirectFunction1MenuItem_Click);
			// 
			// DirectFunction2MenuItem
			// 
			this.DirectFunction2MenuItem.Index = 1;
			this.DirectFunction2MenuItem.Text = "Direct2 (di1)";
			this.DirectFunction2MenuItem.Click += new System.EventHandler(this.DirectFunction2MenuItem_Click);
			// 
			// ColoringMenuItem
			// 
			this.ColoringMenuItem.Index = 1;
			this.ColoringMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																							 this.BlackAndWhiteColoring,
																							 this.ZebraColoring,
																							 this.RainbowColoring,
																							 this.DefaultColoringMenuItem,
																							 this.MandelbrotDefaultColoring});
			this.ColoringMenuItem.Text = "Coloring";
			// 
			// BlackAndWhiteColoring
			// 
			this.BlackAndWhiteColoring.Index = 0;
			this.BlackAndWhiteColoring.Text = "Black/White (Iterative functions only)";
			this.BlackAndWhiteColoring.Click += new System.EventHandler(this.BlackAndWhiteColoring_Click);
			// 
			// ZebraColoring
			// 
			this.ZebraColoring.Index = 1;
			this.ZebraColoring.Text = "Zebra";
			this.ZebraColoring.Click += new System.EventHandler(this.ZebraColoring_Click);
			// 
			// RainbowColoring
			// 
			this.RainbowColoring.Index = 2;
			this.RainbowColoring.Text = "Rainbox (dd1)";
			this.RainbowColoring.Click += new System.EventHandler(this.RainbowColoring_Click);
			// 
			// DefaultColoringMenuItem
			// 
			this.DefaultColoringMenuItem.Index = 3;
			this.DefaultColoringMenuItem.Text = "Default for selected function";
			this.DefaultColoringMenuItem.Click += new System.EventHandler(this.DefaultColoringMenuItem_Click);
			// 
			// MandelbrotDefaultColoring
			// 
			this.MandelbrotDefaultColoring.Index = 4;
			this.MandelbrotDefaultColoring.Text = "Mandelbrot default";
			this.MandelbrotDefaultColoring.Click += new System.EventHandler(this.MandelbrotDefaultColoring_Click);
			// 
			// MouseMenuItem
			// 
			this.MouseMenuItem.Index = 2;
			this.MouseMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						  this.MouseZoomsMenuItem,
																						  this.MouseMovesMenuITem});
			this.MouseMenuItem.Text = "Mouse";
			// 
			// MouseZoomsMenuItem
			// 
			this.MouseZoomsMenuItem.Index = 0;
			this.MouseZoomsMenuItem.Text = "Zoom (recenter, zoom and redraw)";
			this.MouseZoomsMenuItem.Click += new System.EventHandler(this.MouseZoomsMenuItem_Click);
			// 
			// MouseMovesMenuITem
			// 
			this.MouseMovesMenuITem.Index = 1;
			this.MouseMovesMenuITem.Text = "Move (recenter and redraw)";
			this.MouseMovesMenuITem.Click += new System.EventHandler(this.MouseMovesMenuITem_Click);
			// 
			// AdvancedSettingsMenuItem
			// 
			this.AdvancedSettingsMenuItem.Index = 3;
			this.AdvancedSettingsMenuItem.Text = "Function Settings";
			this.AdvancedSettingsMenuItem.Click += new System.EventHandler(this.AdvancedSettingsMenuItem_Click);
			// 
			// About
			// 
			this.About.Index = 2;
			this.About.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																				  this.AboutFZ});
			this.About.Text = "About";
			// 
			// AboutFZ
			// 
			this.AboutFZ.Index = 0;
			this.AboutFZ.Text = "About Fractal Zoomer";
			this.AboutFZ.Click += new System.EventHandler(this.AboutFZ_Click);
			// 
			// MenuItemStart
			// 
			this.MenuItemStart.Index = 3;
			this.MenuItemStart.Text = "Start";
			this.MenuItemStart.Click += new System.EventHandler(this.MenuStart_Click);
			// 
			// StopPaintingMenuItem
			// 
			this.StopPaintingMenuItem.Index = 4;
			this.StopPaintingMenuItem.Text = "Stop painting";
			this.StopPaintingMenuItem.Click += new System.EventHandler(this.StopPaintingMenuItem_Click);
			// 
			// FormUI
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(752, 713);
			this.Controls.Add(this.InfoGroupBox);
			this.Controls.Add(this.Image);
			this.Menu = this.mainMenu1;
			this.Name = "FormUI";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Fractal Zoomer - ";
			this.InfoGroupBox.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new FormUI());
		}

		private void FileExit_Click(object sender, System.EventArgs e)
		{
			// check if the user wants to save, then exit
		}

		private void Image_MouseMove(object sender, MouseEventArgs e)
		{
			this.UpperLeftCornerDataLabel.Text = "(" + painter.OrigoX + "," + painter.OrigoY + ")";
			this.MousePositionInTheWorldDataLabel.Text = "(" + painter.getWX(e.X) + "," + painter.getWY(e.Y) + ")";
			this.ValueOfFDataLabel.Text = Convert.ToString(painter.GetOnePoint(e.X, e.Y));
			this.MousePositionOnScreenDataLabel.Text = "(" + e.X + "," + e.Y + ")";
			this.ScaleDataLabel.Text = Convert.ToString(painter.Scale);

		}

		public void DoMouseDown(object sender, MouseEventArgs args)
		{
			// todo: set what this should do, zoom or move
			painter.StopPainting();
			double newscale = 1D;
			if (ZoomWithMouse) 
			{
				// only change the scale if we're zooming
				newscale = (args.Button == MouseButtons.Left ? 0.75D : 1D/0.75D);
				painter.Rescale(args.X, args.Y, newscale);
			}
			else
			{
				// if we don't save these, we won't know how log we've moved when the button is released, which means we can't move the image
				dragStartX = painter.getWX(args.X);
				dragStartY = painter.getWY(args.Y);
			}
			
		}

		public void DoMouseUp(object sender, MouseEventArgs args)
		{
			if (!ZoomWithMouse) 
			{
				// += works since the number might be nagative anyway, so dragging in a ll directions work.
				painter.OrigoX += (dragStartX - painter.getWX(args.X));
				painter.OrigoY += (dragStartY - painter.getWY(args.Y));
			}
			painter.StartPainting();
		}

		private void MenuStart_Click(object sender, System.EventArgs e)
		{
			painter.StartPainting();
		}
		private void StopPaintingMenuItem_Click(object sender, System.EventArgs e)
		{
			painter.StopPainting();
		}

		private void IterativeDelegate1FunctionMenuItem_Click(object sender, System.EventArgs e)
		{
			this.SelectedFunctionDataLabel.Text = "Iterative delegate 1";
			this.SelectedColoringDataLabel.Text = "Default";
			CheckFunctionMenu((MenuItem)sender);
			id1 = new IterativeDelegate1();
			defaultColorDelegateForCurrentPaintAlgorithm = new ColorDelegate(id1.LookUpColor);
			painter.F = new FDelegate(id1.Function);
			painter.Coloring = defaultColorDelegateForCurrentPaintAlgorithm;
			p = id1;
			this.BlackAndWhiteColoring.Enabled = true;
		}

		private void MandelbrotFunctionMenuItem_Click(object sender, System.EventArgs e)
		{
			this.SelectedFunctionDataLabel.Text = "Iterative Delegate mandelbroot";
			this.SelectedColoringDataLabel.Text = "Default";
			CheckFunctionMenu((MenuItem)sender);
			mandel = new Mandel();
			painter.F = new FDelegate(mandel.MandelFunction);
			defaultColorDelegateForCurrentPaintAlgorithm = new ColorDelegate(mandel.LookUpColor);
			painter.Coloring = defaultColorDelegateForCurrentPaintAlgorithm;
			p = mandel;
			this.BlackAndWhiteColoring.Enabled = true;
			
		}
		private void CheckFunctionMenu(MenuItem m) 
		{
			if (currentlyCheckedFunction != null)
			{
				currentlyCheckedFunction.Checked = false;
			}
			m.Checked = true;
			currentlyCheckedFunction = m;
		}
		private void CheckColoringMenu(MenuItem m) 
		{
			if (currentlyCheckedColoring != null)
			{
				currentlyCheckedColoring.Checked = false;
			}
			m.Checked = true;
			currentlyCheckedColoring = m;
		}

		private void CheckMouseMenu(MenuItem m) 
		{
			if (currentlyCheckedMouse != null)
			{
				currentlyCheckedMouse.Checked = false;
			}
			m.Checked = true;
			currentlyCheckedMouse = m;
		}

		private void AboutFZ_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("Fractal Zoomer 0.1 - Written by Markus Jevring <maje4823@student.uu.se> - December 2004");
		}

		private void MouseMovesMenuITem_Click(object sender, System.EventArgs e)
		{
			CheckMouseMenu((MenuItem)sender);
			ZoomWithMouse = false;
		}

		private void MouseZoomsMenuItem_Click(object sender, System.EventArgs e)
		{
			CheckMouseMenu((MenuItem)sender);
			ZoomWithMouse = true;
		}


		private void DirectFunction1MenuItem_Click(object sender, System.EventArgs e)
		{
			this.SelectedFunctionDataLabel.Text = "Direct Delegate 1";
			this.SelectedColoringDataLabel.Text = "Default";
			CheckFunctionMenu((MenuItem)sender);
			dd1 = new DirectDelegate1();
			painter.F = new FDelegate(dd1.Function);
			defaultColorDelegateForCurrentPaintAlgorithm = new ColorDelegate(dd1.LookUpColor);
			painter.Coloring = defaultColorDelegateForCurrentPaintAlgorithm;
			p = null;
			this.BlackAndWhiteColoring.Enabled = false;

		}

		private void DirectFunction2MenuItem_Click(object sender, System.EventArgs e)
		{
			this.SelectedFunctionDataLabel.Text = "Direct Inheritance 1";
			this.SelectedColoringDataLabel.Text = "Default";
			CheckFunctionMenu((MenuItem)sender);
			painter = new DirectInheritance1(this.Image, origox, origoy, scale);
			defaultColorDelegateForCurrentPaintAlgorithm = new ColorDelegate(painter.LookUpColor);
			p = null;
			this.BlackAndWhiteColoring.Enabled = false;
		}

		private void BlackAndWhiteColoring_Click(object sender, System.EventArgs e)
		{
			// no need to check about being a direct function, since it will be greyed out if it's not.
			this.SelectedColoringDataLabel.Text = "Black And White";
			CheckColoringMenu((MenuItem)sender);
			painter.Coloring = new ColorDelegate(p.BAW);
		}

		private void ZebraColoring_Click(object sender, System.EventArgs e)
		{
			this.SelectedColoringDataLabel.Text = "Zebra";
			CheckColoringMenu((MenuItem)sender);
			painter.Coloring = new ColorDelegate(painter.Zebra);
		}

		private void DefaultColoringMenuItem_Click(object sender, System.EventArgs e)
		{
			this.SelectedColoringDataLabel.Text = "Default";
			CheckColoringMenu((MenuItem)sender);
			painter.Coloring = defaultColorDelegateForCurrentPaintAlgorithm;

		}

		private void AdvancedSettingsMenuItem_Click(object sender, System.EventArgs e)
		{
			AdvancedSettings advs = new AdvancedSettings(painter);
			DialogResult dr = advs.ShowDialog();
			ColorDelegate tc = painter.Coloring;
			FDelegate tf = painter.F;
			if (dr == DialogResult.OK) 
			{
				try 
				{

					origox = Convert.ToDouble(advs.OrigoX);
					origoy = Convert.ToDouble(advs.OrigoY);
					scale = Convert.ToDouble(Convert.ToDouble(advs.MyScale).ToString(".0000000000000000000000000"));
					
					painter = new TheWorldPainter(this.Image, origox, origoy, scale);
					painter.F = tf;
					painter.Coloring = tc;
				}
				catch (Exception ex)
				{
					MessageBox.Show("Something went wrong: " + ex.Message);
				}
				if (p != null && !"".Equals(advs.MaxIterations))
				{
					// only for iterative functions
					p.MaxIterations = Convert.ToInt32(advs.MaxIterations);
				}
			}
		}

		private void FileSaveAs_Click(object sender, System.EventArgs e)
		{
			SaveFileDialog sfd = new SaveFileDialog();
			DialogResult dr = sfd.ShowDialog();
			if (dr == DialogResult.OK) 
			{
				SaveFileName = sfd.FileName;
			}
			SaveFile();

		}
		private void SaveFile()
		{
			FileStream fs = new FileStream(SaveFileName, FileMode.Create);
			SoapFormatter sf = new SoapFormatter();
			sf.Serialize(fs, painter);
		}

		private void FileSave_Click(object sender, System.EventArgs e)
		{
			// in case we haven't selected a name yet
			if ("".Equals(SaveFileName))
			{
				FileSaveAs_Click(null, null);
			}
			SaveFile();
		}

		private void FileLoad_Click(object sender, System.EventArgs e)
		{
			OpenFileDialog ofd = new OpenFileDialog();
			DialogResult dr = ofd.ShowDialog();
			if (dr == DialogResult.OK) 
			{
				SaveFileName = ofd.FileName;
			}
			LoadFile();
		}
		private void LoadFile()
		{
			painter.StopPainting();
			FileStream fs = new FileStream(SaveFileName, FileMode.Open);
			SoapFormatter sf = new SoapFormatter();
			// this works even for the subclasses.
			TheWorldPainter t = (TheWorldPainter)sf.Deserialize(fs);
			//since this isn't going to work, since we can't serialize our GUI controls, we're going into sickhack mode!
			painter = new TheWorldPainter(this.Image, t.OrigoX, t.OrigoY, t.Scale);
			painter.F = t.F;
			painter.Coloring = t.Coloring;
			painter.StartPainting();
			
		}

		private void FormUI_MouseWheel(object sender, MouseEventArgs e)
		{
			painter.StopPainting();
			if (e.Delta > 0)
			{
				painter.Rescale(Image.Width / 2, Image.Height / 2, 0.75D);
			}
			else
			{
				painter.Rescale(Image.Width / 2, Image.Height / 2, 1D/0.75D);
			}
			painter.StartPainting();
		}

		private void RainbowColoring_Click(object sender, System.EventArgs e)
		{
			painter.Coloring = new ColorDelegate((new DirectDelegate1()).LookUpColor);
		}

		private void MandelbrotDefaultColoring_Click(object sender, System.EventArgs e)
		{
			painter.Coloring = new ColorDelegate((new Mandel()).LookUpColor);
		}
	}
}
