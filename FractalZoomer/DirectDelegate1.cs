using System;
using System.Drawing;

namespace FractalZoomer
{
	/// <summary>
	/// Summary description for DirectDelegate1.
	/// </summary>
	[Serializable] 
	public class DirectDelegate1
	{
		public DirectDelegate1()
		{
		}
		public int Function(double cre, double cim) 
		{
			Complex c = new Complex(cre, cim);
			Complex z = (3*c*c+2*c-1)*(1-c)/(1 + c + c*c + c*c*c) + (10*c*c*c*c + 8*c*c*c -12*c*c + 24*c - 13);
			//Complex z = (3*c*c+2*c-1)*(1-c)/(1 + c + c*c + c*c*c);
			// this generates very nice colors when used
			//Complex z = 10*c*c*c*c + 8*c*c*c -12*c*c + 24*c - 13;
			
			return (int) z.Norm;
		}
		
		public Color LookUpColor(int n)
		{
			return Color.FromArgb(Math.Abs(n)%256,Math.Abs(255-n)%255,Math.Abs(128-n)%255);
			//return (n%3 == 1 ? Color.Black : Color.White);
		}
	}
}
